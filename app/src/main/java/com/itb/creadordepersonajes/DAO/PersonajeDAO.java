package com.itb.creadordepersonajes.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.itb.creadordepersonajes.model.Personaje;

import java.util.List;

@Dao
public interface PersonajeDAO {

    @Insert
    public void insert(Personaje personaje);

    @Query("SELECT * FROM personajes")
    public LiveData<List<Personaje>> getPersonajes();

    @Query("SELECT * FROM personajes WHERE id = :id")
    public LiveData<List<Personaje>> getPersonajesById(int id);

    @Update
    void update(Personaje personaje);

    @Delete
    void delete(Personaje personaje);

}

package com.itb.creadordepersonajes.views;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itb.creadordepersonajes.R;

public class PantallaDeCreacionDePersonaje extends Fragment {

    private PantallaDeCreacionDePersonajeViewModel mViewModel;

    public static PantallaDeCreacionDePersonaje newInstance() {
        return new PantallaDeCreacionDePersonaje();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pantalla_de_creacion_de_personaje_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PantallaDeCreacionDePersonajeViewModel.class);
        // TODO: Use the ViewModel
    }

}

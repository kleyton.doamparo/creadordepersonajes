package com.itb.creadordepersonajes.views;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itb.creadordepersonajes.R;
import com.itb.creadordepersonajes.adapter.PersonajeAdapter;
import com.itb.creadordepersonajes.model.Personaje;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PantallaPrincipal extends Fragment {

    @BindView(R.id.recyclerviewListaPersonajes)
    RecyclerView personajeRecyclerView;
    private PantallaPrincipalViewModel mViewModel;
    PersonajeAdapter adapter;

    public static PantallaPrincipal newInstance() {
        return new PantallaPrincipal();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.panatalla_principal_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PantallaPrincipalViewModel.class);

        personajeRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        personajeRecyclerView.setLayoutManager(layoutManager);

        adapter = new PersonajeAdapter();
        personajeRecyclerView.setAdapter(adapter);
        adapter.setListener(this::viewPersonaje);
        LiveData<List<Personaje>> personajes = mViewModel.getPersonajes();
        personajes.observe(this,this::onPersonajeChanged);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void viewPersonaje(Personaje personaje) {
        Integer idPersonaje = Integer.valueOf(personaje.getId());
        NavDirections direction = PantallaPrincipalDirections.viewPersonajeDetail();
        Navigation.findNavController(this.getView()).navigate(direction);
    }

    private void onPersonajeChanged(List<Personaje> personajes) {
        adapter.setPersonajes(personajes);
        if (personajes.isEmpty()){
            Toast.makeText(getContext(), "No hay Personajes", Toast.LENGTH_SHORT).show();

        }
    }

    @OnClick(R.id.btnAddCharacter)
    public void onViewClicked(){
        Navigation.findNavController(personajeRecyclerView).navigate(R.id.viewPersonajeCreation);
    }



}

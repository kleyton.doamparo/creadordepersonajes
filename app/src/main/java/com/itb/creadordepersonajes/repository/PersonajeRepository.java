package com.itb.creadordepersonajes.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.itb.creadordepersonajes.DAO.PersonajeDAO;
import com.itb.creadordepersonajes.db.ApplicationDataBase;
import com.itb.creadordepersonajes.model.Personaje;

import java.util.List;

public class PersonajeRepository {
    private PersonajeDAO dao;
    private LiveData<List<Personaje>> personajes;

    public PersonajeRepository(Application context) {
        ApplicationDataBase db = ApplicationDataBase.getINSTANCE(context);
        this.dao=db.getDAO();
        personajes=dao.getPersonajes();
    }

    public void savePersonaje(Personaje personaje) { new InsertAsyncTask(dao).execute(personaje); }
    private static class InsertAsyncTask extends AsyncTask<Personaje, Void, Void> {
        PersonajeDAO mDao;
        public InsertAsyncTask(PersonajeDAO dao) {
            mDao=dao;
        }

        @Override
        protected Void doInBackground(Personaje... personajes) {
            mDao.insert(personajes[0]);
            return null;
        }
    }

    public void updatePersonaje(Personaje personaje) { new UpdateAsyncTask(dao).execute(personaje); }
    private class UpdateAsyncTask extends AsyncTask<Personaje, Void, Void> {
        PersonajeDAO dao;
        public UpdateAsyncTask(PersonajeDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Personaje... personajes) {
            dao.update(personajes[0]);
            return null;
        }
    }

    public void deletePersonaje(Personaje personaje) { new DeleteAsyncTask(dao).execute(personaje); }
    private class DeleteAsyncTask extends AsyncTask<Personaje, Void, Void>{
        PersonajeDAO dao;
        public DeleteAsyncTask(PersonajeDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(Personaje... personajes) {
            dao.delete(personajes[0]);
            return null;
        }
    }
    public LiveData<List<Personaje>> getPersonajes() {
        return personajes;
    }
    public LiveData<List<Personaje>> getPersonajeById(int idPersonaje) {
        return dao.getPersonajesById(idPersonaje);
    }
}

package com.itb.creadordepersonajes.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity (tableName = "personajes")
public class Personaje {
    @PrimaryKey(autoGenerate = true)
    int id;
    String nombre;
    int edad;
    String origen;
    String genero;
    String pasado;
    String raza;
    String actitud;
    String armas;
    String items;
    String virtud;

    public Personaje(int id, String nombre, int edad, String origen, String genero, String pasado, String raza, String actitud, String armas, String items, String virtud) {
        this.id = id;
        this.nombre = nombre;
        this.edad = edad;
        this.origen = origen;
        this.genero = genero;
        this.pasado = pasado;
        this.raza = raza;
        this.actitud = actitud;
        this.armas = armas;
        this.items = items;
        this.virtud = virtud;
    }

    @Ignore
    public Personaje(int id){this.id=id;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getPasado() {
        return pasado;
    }

    public void setPasado(String pasado) {
        this.pasado = pasado;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getActitud() {
        return actitud;
    }

    public void setActitud(String actitud) {
        this.actitud = actitud;
    }

    public String getArmas() {
        return armas;
    }

    public void setArmas(String armas) {
        this.armas = armas;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getVirtud() {
        return virtud;
    }

    public void setVirtud(String virtud) {
        this.virtud = virtud;
    }
}



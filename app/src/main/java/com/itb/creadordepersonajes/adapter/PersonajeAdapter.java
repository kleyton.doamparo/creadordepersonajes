package com.itb.creadordepersonajes.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.creadordepersonajes.R;
import com.itb.creadordepersonajes.model.Personaje;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonajeAdapter extends RecyclerView.Adapter<PersonajeAdapter.PersonajeViewHolder> {
    private List<Personaje> personajes = new ArrayList<>();
    OnCharacterClickListener listener;

    public PersonajeAdapter() {
    }



    public void setPersonajes(List<Personaje> personajes) {
        this.personajes = personajes;
        notifyDataSetChanged();
    }

    public void setListener(OnCharacterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public PersonajeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fila_personaje_row, parent, false);
        return new PersonajeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonajeViewHolder holder, int position) {
        Personaje personaje = personajes.get(position);
        holder.nombreItemTextView.setText(personaje.getNombre());
        holder.generoItemTextView.setText(personaje.getGenero());
        holder.razaItemTextView.setText(personaje.getRaza());
    }

    @Override
    public int getItemCount() {
        int size=0;
        if (personajes !=null) size= personajes.size();
        return size;
    }

    public class PersonajeViewHolder extends  RecyclerView.ViewHolder{

        @BindView(R.id.nombreItemTextView)
        TextView nombreItemTextView;
        @BindView(R.id.generoItemTextView)
        TextView generoItemTextView;
        @BindView(R.id.razaItemTextView)
        TextView razaItemTextView;

        public PersonajeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.fila_personaje_row)
        public void onCharacterViewClicked(){
            Personaje personaje = personajes.get(getAdapterPosition());
            listener.onCharacterViewClicked(personaje);
        }
    }

    public interface OnCharacterClickListener {
        void onCharacterViewClicked (Personaje personaje);
    }
}

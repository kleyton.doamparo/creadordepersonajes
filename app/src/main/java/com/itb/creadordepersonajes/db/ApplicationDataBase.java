package com.itb.creadordepersonajes.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.itb.creadordepersonajes.DAO.PersonajeDAO;
import com.itb.creadordepersonajes.model.Personaje;

public abstract class ApplicationDataBase extends RoomDatabase {
    private static volatile ApplicationDataBase INSTANCE;
    private static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE 'personajes' ADD COLUMN 'adapted' INTEGER NOT NULL DEFAULT 0");
        }
    };

    public static ApplicationDataBase getINSTANCE(Context context){
        if (INSTANCE==null){
            synchronized (ApplicationDataBase.class){
                if (INSTANCE==null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ApplicationDataBase.class, "personajes_database").addMigrations(MIGRATION_1_2).build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract PersonajeDAO getDAO();
}

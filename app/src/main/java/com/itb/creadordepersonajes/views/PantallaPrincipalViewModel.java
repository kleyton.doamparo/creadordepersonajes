package com.itb.creadordepersonajes.views;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.itb.creadordepersonajes.model.Personaje;
import com.itb.creadordepersonajes.repository.PersonajeRepository;

import java.util.List;

public class PantallaPrincipalViewModel extends AndroidViewModel {
    private PersonajeRepository personajeRepository;
    private LiveData<List<Personaje>> personajes;
    private boolean isUpdate=false;

    public PantallaPrincipalViewModel(@NonNull Application application) {
        super(application);
        personajeRepository = new PersonajeRepository(application);
        personajes = personajeRepository.getPersonajes();
    }

    public void savePersonaje(Personaje personaje) {
        if (isUpdate){
            personajeRepository.updatePersonaje(personaje);
        }
        else{personajeRepository.savePersonaje(personaje);}
    }

    public LiveData<List<Personaje>> getPersonajes() {
        return personajes;
    }


    public boolean isUpdate(int idPersonaje) {
        isUpdate = idPersonaje !=-1;
        return isUpdate;
    }

    public LiveData<List<Personaje>> getPersonajeById(int idPersonaje) {
        return personajeRepository.getPersonajeById(idPersonaje);
    }
}
